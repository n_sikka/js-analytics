(function(){
  'use strict';

  angular
    .module('ng-app')
    .service('MatchService', matchService)

    function matchService(DATA_CSV, API, $http, $q){

      var service = this;

      //one time function for db creation on a new maching
      service.createDb = function(){

        if(localStorage.getItem('dbCreated')){

          console.log('db is present, if its an error clear localstorage first and re run app');

        } else {

          console.log('db doesnt exist, creating db...');

          $http.get(DATA_CSV)
          .then(function success(response){

            Papa.parse(response.data, {
              header: true,
              complete: function(results) {
                service.postToDb(results.data)
              }
            })

          }, function error(response){

            console.log(response);

          })

        }


      },


      service.postToDb = function(req) {

          $http.post(API.MATCHES_POST, req)
          .then(function success(response){

            localStorage.setItem('dbCreated', true);

          }, function success(response){
            console.log(response);
          })

      },


      service.getAll = function(){
        return $http({
          method: 'GET',
          cache: true,
          url: API.MATCHES_GET
        })
      },


      service.getRunsStat = function(){
        var deferred = $q.defer();

        service.getAll()
        .then(function success(response){

          var res = response.data,
              totalRunsArr = [],
              totalRuns = 0,
              outCount = 0,
              notOutCount = 0;

          for (var i = 0; i < res.length; i++) {

            if(!isNaN(parseInt(res[i].batting_score, 10))){

              if(res[i].batting_score.includes('*')){

                var temp = +parseInt(res[i].batting_score, 10) || 0
                totalRunsArr.push(temp)
                notOutCount++

              }else {

                var temp = +parseInt(res[i].batting_score, 10) || 0
                totalRunsArr.push(temp)
                outCount++;

              }

            }
            //end of forloop
          }

          //get total runs (sum)
          function getSum(total, num) {
              return total + num;
          }

          var stats = {
            runs: totalRunsArr.reduce(getSum),
            avg: parseFloat((totalRunsArr.reduce(getSum)/outCount).toFixed(2)),
            notOut: notOutCount
          }

          deferred.resolve(stats)

        }, function error(response){
          console.log(response.statusText);
        })

        return deferred.promise;

      },


      service.getCenturies = function(){
        var deferred = $q.defer();

        service.getAll()
          .then(function success(response){

            var res = response.data,
                century = {
                  score: 0,
                  opponent: ''
                },
                centuryCount = 0,
                totalCenturies = [];

                for (var i = 0; i < res.length; i++) {

                  if(!isNaN(parseInt(res[i].batting_score, 10))) {

                    if(parseInt(res[i].batting_score, 10) >= 100) {
                      century = {
                        score: res[i].batting_score,
                        opponent: res[i].opposition
                      }
                      totalCenturies.push(century)
                      centuryCount++;
                    }

                  }

                }

                var response = {
                  centuries: totalCenturies,
                  count: centuryCount
                }

                deferred.resolve(response);

          }, function error(response){
            console.log(response.statusText);
          })

          return deferred.promise;

      },


      service.getHalfCenturies = function(){

        var deferred = $q.defer();

        service.getAll()
          .then(function success(response){

            var res = response.data,
                halfCentury = {
                  score: 0,
                  opponent: ''
                },
                halfCenturyCount = 0,
                totalHalfCenturies = [];

                for (var i = 0; i < res.length; i++) {

                  if(!isNaN(parseInt(res[i].batting_score, 10))) {

                    if(parseInt(res[i].batting_score, 10) >= 50 && parseInt(res[i].batting_score, 10) <= 100) {
                      halfCentury = {
                        score: res[i].batting_score,
                        opponent: res[i].opposition
                      }
                      totalHalfCenturies.push(halfCentury)
                      halfCenturyCount++;
                    }

                  }

                }

                var response = {
                  half_centuries: totalHalfCenturies,
                  count: halfCenturyCount
                }

                deferred.resolve(response);

          }, function error(response){
            console.log(response.statusText);
          })

          return deferred.promise;

      },


      service.getMatchResults = function(){
        var deferred = $q.defer();

        service.getAll()
          .then(function success(response){

            var res = response.data,
                matchResults = [],
                totalLost = 0,
                totalWon = 0,
                totalTied = 0,
                otherRes = 0;

            for (var i = 0; i < res.length; i++) {
              if(res[i].match_result === 'won'){
                totalWon++
              } else if(res[i].match_result === 'lost'){
                totalLost++
              } else if(res[i].match_result === 'tied'){
                totalTied++
              } else {
                otherRes++
              }
            }

            var matchStats = {
              won: totalWon,
              lost: totalLost,
              tied: totalTied,
              no_result:otherRes
            }

            deferred.resolve(matchStats)

          }, function error(response){

          })

          return deferred.promise;
      },


      service.getStatsByYear = function(){
        var deferred = $q.defer();

        service.getAll()
          .then(function success(response){

            var res = response.data,
                centuriesByYr = [],
                halfCenturiesByYr = [],
                wonByYr = [],
                lostByYr = []


            for (var i = 0; i < res.length; i++) {
              if(res[i].match_result === 'won'){

              } else if(res[i].match_result === 'lost'){
                totalLost++
              } else if(res[i].match_result === 'tied'){
                totalTied++
              } else {
                otherRes++
              }
            }

            var matchStats = {
              won: totalWon,
              lost: totalLost,
              tied: totalTied,
              no_result:otherRes
            }

            deferred.resolve(matchStats)

          }, function error(response){

          })

          return deferred.promise;
      },


      service.getCenturyByYear = function(){

        var deferred = $q.defer();

        service.getAll()
            .then(function success(response){

              var data = response.data;

              var centuries = []
              for (var i = 0; i < data.length-1; i++) {
                var key = (new Date(Date.parse(data[i].date))).getFullYear()


                if (typeof(centuries[key]) == "undefined")
                  centuries[key] = [];

                if(!isNaN(parseInt(data[i].batting_score, 10))) {

                  if(parseInt(data[i].batting_score, 10) >= 100) {

                    var obj = {
                      score : data[i].batting_score,
                      opponent : data[i].opposition
                    }
                    centuries[key].push(obj);

                  }

                }

              }

              deferred.resolve(centuries)

            }, function error(response){
              console.log(response);
            })

            return deferred.promise;

      },


      service.getHalfCenturiesByYear = function(){
        var deferred = $q.defer();

        service.getAll()
            .then(function success(response){

              var data = response.data;

              var centuries = []
              for (var i = 0; i < data.length-1; i++) {
                var key = (new Date(Date.parse(data[i].date))).getFullYear()


                if (typeof(centuries[key]) == "undefined")
                  centuries[key] = [];

                if(!isNaN(parseInt(data[i].batting_score, 10))) {

                  if(parseInt(data[i].batting_score, 10) >= 50 && parseInt(data[i].batting_score, 10) < 100) {

                    var obj = {
                      score : data[i].batting_score,
                      opponent : data[i].opposition
                    }
                    centuries[key].push(obj);

                  }

                }

              }

              deferred.resolve(centuries)

            }, function error(response){
              console.log(response);
            })

            return deferred.promise;
      }


      service.getResultByYear = function(){
        var deferred = $q.defer();

        service.getAll()
            .then(function success(response){

              var data = response.data;

              var results = []

              for (var i = 0; i < data.length-1; i++) {
                var key = (new Date(Date.parse(data[i].date))).getFullYear()


                if (typeof(results[key]) == "undefined")
                  results[key] = [];

                if(data[i].match_result !== 'n/r') {
                  results[key].push(data[i].match_result);
                }

              }

              deferred.resolve(results)

            }, function error(response){
              console.log(response);
            })

            return deferred.promise;
      },



      //to do
      service.getWinsByCenturies = function(){

      },


      service.getWinsByHalfCenturies = function(){

      },


      service.getLossByCenturies = function(){

      },


      service.getLossByHalfCenturies = function(){

      }

      return service;

      // var temp = Date.parse(res[i].date)
      // var date = new Date(temp)
      // var matchDate = date.toLocaleDateString("en-US")

    }

})();
