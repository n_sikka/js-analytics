(function(){

  angular
  .module('ng-app')
  .controller('HomeController' , controller);

  function controller(MatchService){
    var vm = this;

    MatchService.createDb();

    // MatchService.getAll().then(function(response){
    //
    //
    // })

    MatchService.getCenturies().then(function(result){
      console.log(result);
    });

    MatchService.getRunsStat().then(function(result){
      console.log(result);
    });

    MatchService.getHalfCenturies().then(function(result){
      console.log(result);
    });

    MatchService.getMatchResults().then(function(result){
      console.log(result);
    })

    MatchService.getCenturyByYear().then(function(result){
      console.log('centuries by year', result);
    })

    MatchService.getHalfCenturiesByYear().then(function(result){
      console.log('half centuries by year', result);
    })

    MatchService.getResultByYear().then(function(result){
      console.log('result by year', result);
    })
  }


})();
