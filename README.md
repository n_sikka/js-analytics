# JS Analytics

## Quick Start

Install Node.js and then:

- git clone git@bitbucket.org:n_sikka/js-analytics.git
- cd js-analytics
- sudo npm -g install gulp bower
- npm install
- bower install
- gulp serve

Install MongoDb (ubuntu)

- sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
- sudo apt-get update
- sudo apt-get install -y mongodb-org

Set up LocalServer
- open terminal
- cd js-analytics/API
- node server

Start MongoDb
- sudo service mongodb

Open MongoDB Shell
- just type -> mongo


Install Mongo (OSX)
- http://www.bigspaceship.com/mongodb-on-mac/

App will be live on localhost:3000

# Tools used
- Gulp
- NPM
- Bower
- LiveReload
