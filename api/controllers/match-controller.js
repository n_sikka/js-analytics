var Match = require('../models/match');

module.exports.add = function(req, res) {

  var matchArray = []

  for (var i = 0; i < req.body.length; i++) {
    var match = new Match(req.body[i]);
    matchArray.push(match)
  }

  Match.create(matchArray, function (err, matches) {
    if (err) {
      console.log(err);
      res.json({status: 500})
    } else {
      console.log('DB Created');
      res.json({status: 200});
    }

  });

}


module.exports.fetch = function(req, res) {

  Match.find({}, '-_id', function(err, matches){
    if(err) {
      console.log(err);
      res.json({status: 500})
    } else {
      res.json(matches)
    }
  })

}
