var mongoose = require('mongoose');


var Schema = mongoose.Schema;


var matchSchema = new Schema({

  batting_score: String,

  wickets: String,

  runs_conceded: String,

  catches: String,

  stumps: String,

  opposition: String,

  ground: String,

  date: Date,

  match_result: String,

  result_margin: String,

  toss: String,

  batting_innings: String,

}, { versionKey: false });


var Match = mongoose.model('Match', matchSchema);


module.exports = Match;
