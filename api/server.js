var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');


var matchController = require('./controllers/match-controller');



var app = express();

mongoose.connect('mongodb://localhost:27017/social-cops');

//for parsing json data (otherwise it gives out undefined on any external request on server side)

app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser({limit: '50mb'}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//rest api index (not needed here)
// app.get('/', function(req, res){
//
// 	res.sendFile(path.join(__dirname, 'index.html'));
//
// })

//Set up server for this app at 3000 port
app.listen('3001', function(){

	console.log('Server Started at localhost:3001');

});


//Match
app.post('/match/post', matchController.add);
app.get('/match/get', matchController.fetch);



// disabling 404 message for dev
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
// 		err.status = 404;
//     next(err);
// });
